#!/usr/bin/python
# -*- coding: utf-8; mode: python -*-

import unittest
import os

version = ['15.05', '16.04']
architectures = ['x86', 'imx6', 'ar71xx']
branch = ['tecos', 'tecteco-custom']
label = ['experimental', 'nightly', 'testing', 'stable']


class TestRepo(unittest.TestCase):

    def test_hier_previous(self):
        for v in version:
            for a in architectures:
                for l in label:
                    for b in branch:
                        print os.path.join(os.getcwd(), "development", v, a, l, b, "incoming")
                        self.assertTrue(os.path.exists(os.path.join(os.getcwd(), "development", v, a, b, l, "incoming")))
                        self.assertFalse(os.path.exists(os.path.join(os.getcwd(), "development", v, l, "incoming")))
                        self.assertFalse(os.path.exists(os.path.join(os.getcwd(), v, a, b)))
                        self.assertFalse(os.path.exists(os.path.join(os.getcwd(), v, a, b, l, "incoming")))
                        self.assertFalse(os.path.exists(os.path.join(os.getcwd(), v, a, l, "incoming")))


if __name__ == '__main__':
    unittest.main()
