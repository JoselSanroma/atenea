#!/usr/bin/python
# -*- coding: utf-8; mode: python -*-

import SimpleHTTPServer
import SocketServer
import subprocess

PORT = 8000

Handler = SimpleHTTPServer.SimpleHTTPRequestHandler

httpd = SocketServer.TCPServer(("", PORT), Handler)

print "serving at port", PORT

subprocess.call(["create-opkg-index"])

httpd.serve_forever()
