#!/bin/bash --
# -*- coding: utf-8; mode: shell-script; tab-width: 4 -*-

if [ -f Packages ] || [ -f Pacakges.gz ]
then
    rm Packages*
fi

ipkg-make-index . > Packages
gzip -9nc Packages > Packages.gz
