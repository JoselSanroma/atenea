#!/bin/bash --
# -*- coding: utf-8; mode: shell-script; tab-width: 4 -*-

setfacl -R -m group:builder:7 /var/repos/owrt/development/17.01.4/packages/i386_pentium4/tecos/nightly/incoming/


setfacl -R -m group:builder:7 /var/repos/owrt/development/17.01.4/packages/arm_cortex-a15_neon-vfpv4/tecos/nightly/incoming/


setfacl -R -m group:builder:7 /var/repos/owrt/development/17.01.4/targets/ipq806x/generic

setfacl -R -m group:builder:7 /var/repos/owrt/development/17.01.4/targets/x86/generic/
