#!/bin/bash --
# -*- coding: utf-8; mode: shell-script; tab-width: 4 -*-

# Be aware, by now ALEJANDRIA is not testing any suite. Do not change
# that unless you have talked with the TECTECO Software Development.

USER="mirrorer"
ALEJANDRIA="10.10.150.101"
ORIG="/var/repos/owrt/development/17.01.4/"
SUITE="testing"
DEST="/var/repos/17.01.4/"

rsync -vrltpqHS --delete-excluded --exclude local-sources --exclude "tecos" \
      --exclude "old" $ORIG $USER@$ALEJANDRIA:$DEST
