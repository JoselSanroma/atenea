#!/bin/bash --
# -*- coding: utf-8; mode: shell-script; tab-width: 4 -*-

LEDE_DIR="/var/repos/lede"
OWRT_DIR="/var/repos/owrt"

function create_owrt_hierarchy(){
    cd $OWRT_DIR
    mkdir -p development/{15.05,16.04}/{x86,ar71xx,imx6}/{tecos,tecteco-custom}/{experimental,testing,nightly,stable}/incoming

}

function create_lede_hierarchy(){
    cd $LEDE_DIR
    mkdir -p development/{17.01.4}/{ipq806x}/{tecos}/{testing,nightly,stable}/incoming
}


if [ -d $OWRT_DIR && -d $LEDE_DIR ]
then
    create_owrt_hierarchy
    create_lede_hierarchy
else
    if [ ! -d $OWRT_DIR || ! -d $LEDE_DIR ]
    then
        echo "bad directory tree"
    fi
fi
