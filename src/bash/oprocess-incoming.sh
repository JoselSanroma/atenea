#!/bin/bash --
# -*- coding: utf-8; mode: shell-script; tab-width: 4 -*-

REPO_PATH="/var/repos/owrt/"

POOL=("development/17.01.4/packages/arm_cortex-a15_neon-vfpv4/tecos/experimental"
      "development/17.01.4/packages/arm_cortex-a15_neon-vfpv4/tecos/nightly"
      "development/17.01.4/packages/arm_cortex-a9_vfpv3/tecos/experimental"
      "development/17.01.4/packages/arm_cortex-a9_vfpv3/tecos/nightly"
      "development/17.01.4/packages/i386_pentium4/tecos/nightly"
      "development/17.01.4/packages/i386_pentium4/tecos/testing")

function create_index(){
    for i in "${POOL[@]}"
    do
        target="$REPO_PATH$i"
        cd $target && create-opkg-index
            if [ $? -eq 0 ]
            then
                echo -e "Created index $target ... [OK]"
            fi
    done
}

function process_packages_queue(){
    atenea processincoming -v 17.01.4 -a arm_cortex-a15_neon-vfpv4 -l experimental &
    atenea processincoming -v 17.01.4 -a arm_cortex-a15_neon-vfpv4 -l nightly &
    atenea processincoming -v 17.01.4 -a arm_cortex-a15_neon-vfpv4 -l testing &
    atenea processincoming -v 17.01.4 -a arm_cortex-a9_vfpv3 -l experimental &
    atenea processincoming -v 17.01.4 -a arm_cortex-a9_vfpv3 -l nightly &
    atenea processincoming -v 17.01.4 -a i386_pentium4 -l nightly &
    atenea processincoming -v 17.01.4 -a i386_pentium4 -l testing &
    wait

}


process_packages_queue
create_index
