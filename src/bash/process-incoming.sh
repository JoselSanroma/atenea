#!/bin/bash --
# -*- coding: utf-8; mode: shell-script; tab-width: 4 -*-

REP_PATH="/var/repos/tecteco"

function process_incoming(){
    cd $REP_PATH
    reprepro processincoming sid-process
}

process_incoming
