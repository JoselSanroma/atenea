#!/bin/bash --
# -*- coding: utf-8; mode: shell-script; tab-width: 4 -*-
# This script is intended to do some maintenance operations for reprepro:
# - dumpunreferenced
# This lists all files reprepro knows about that are not marked as
# needed by anything. Unless you called reprepro with the
# --keepunreferenced option, those should never occur. Though if
# reprepro is confused or interrupted it may sometimes prefer keeping
# files around instead of deleting them.

# - deleteunreferenced This is like the command before, only that such
# files are directly forgotten and deleted.

# - check
# Look if all needed files are in fact marked needed and known.

# - checkpool
# Make sure all known files are still there and still have the same
# checksum.

# - tidytracks
# If you use source package tracking, check for files kept
# because of this that should no longer by the current rules.

# retrack
# That refreshes the tracking information about packages used
# and then runs a tidytracks.

BASE_DIR="/var/repos/tecteco"
REPREPRO_OPERATIONS=("dumpunreferenced"
            "deleteunreferenced"
            "check"
            "checkpool"
            "tidytracks"
            "retrack")

function maintenance(){

    for op in "${REPREPRO_OPERATIONS[@]}"
    do
        echo $op
        reprepro -V -b $BASE_DIR $op
    done
}
maintenance
