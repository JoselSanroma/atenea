#!/bin/bash --
# -*- coding: utf-8; mode: shell-script; tab-width: 4 -*-

# Be aware, by now testing is the default suite. Do not change that
# unless you have talked with the TECTECO Software Development.

archs=(arm_cortex-a15_neon-vfpv4 i386_pentium4)
USER="mirrorer"
PERGAMO_CLOUD="10.10.30.3"
ORIG="/var/repos/owrt/development/17.01.4/"
DEST="/var/repos/17.01.4/"


rsync -vrltpqHS --delete-excluded --include="targets/ipq806x/generic/packages" \
      --exclude="targets/ipq806x/generic/*" --exclude local-sources \
      --exclude incoming --exclude experimental --exclude i386_pentium4 --exclude x86 \
      $ORIG $USER@$PERGAMO_CLOUD:$DEST
