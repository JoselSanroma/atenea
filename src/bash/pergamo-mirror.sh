#!/bin/bash --
# -*- coding: utf-8; mode: shell-script; tab-width: 4 -*-

# Be aware, by now testing is the default suite. Do not change that
# unless you have talked with the TECTECO Software Development.

archs=(arm_cortex-a15_neon-vfpv4 i386_pentium4)
USER="mirrorer"
PERGAMO="10.10.150.78"
ORIG="/var/repos/owrt/development/17.01.4/"
SUITE="testing"
DEST="/var/repos/17.01.4/"


rsync -vrltpqHS --delete-excluded --exclude local-sources --exclude "tecos" \
      --exclude "old" $ORIG $USER@$PERGAMO:$DEST/

for i in "${archs[@]}"
do
    rsync -vrltpqHS --delete-excluded --exclude local-sources \
          $ORIG/packages/$i/tecos/$SUITE/* $USER@$PERGAMO:$DEST/packages/$i/tecos
done
