#!/usr/bin/python
# -*- coding: utf-8; mode: python -*-
'''
This file contains  the needed Class to manage the Openwrt repository
'''
import os
import sys
import shutil
import argparse
import subprocess
import configparser

ARCH_LIST = ['arm_cortex-a15_neon-vfpv4',
             'i386_pentium4',
             'arm_cortex-a9_vfpv3']

LEDE_VERSION = "17.01.4"


class OpkgPackage():
    '''
    Opkg package class.
    '''

    def __init__(self, name, version, arch, extension=None):
        '''
        Constructor.
        Args:
            name: package name
            version: package version
            arch: package architecture
            extension: package extension
        '''
        self.name = name
        self.version = version
        self.arch = arch
        self.extension = extension
        self.delete = False

    def __eq__(self, other):
        '''
        Equal (__eq__) object comparison function. One package is
        equal to one another if and only if name and arch are equal to
        each others.
        Args:
            other: other package to compare
        '''
        return (self.name == other.name) and (self.arch == other.arch) \
            if isinstance(other, OpkgPackage) else False

    def __gt__(self, other):
        '''
        Greater than (__gt__) object comparison function. One package
        is greater than one another if and only if package version is
        greater and the name and the architecture are equal to each others.
        Args:
            other: other package to compare
        '''
        return ((self.name == other.name) and (self.version > other.version)
                and (self.arch == other.arch)) if isinstance(other, OpkgPackage) \
                else False

    def __ne__(self, other):
        '''
        Not equal (__ne__) object comparison function.
        Args:
            other: other package to compare
        '''
        return not self.__eq__(other)

    def __repr__(self):
        '''
        Built-in function to compute the official string representation of an
        object.
        '''
        return 'OpkpgPackage(name=%s, arch=%s)\n' % (self.name, self.arch)

    def __hash__(self):
        '''
        This function ios needed because of the __eq__ funtion
        '''
        return hash(self.__repr__())


class Orepo():
    '''
    Class to manage opkg packages.
    '''
    def __init__(self, args_list):
        '''
        Constructor.
        Args:
            Args: Command line args
        '''

        self.args = args_list

        config = configparser.ConfigParser()

        repo_path = ""
        if args.t:
            config.read_file(open(r'../test/orepo.conf'))
            repo_path = config.get('OREPO', 'path')

        else:
            config.read_file(open(r'/etc/orepo/orepo.conf'))
            repo_path = config.get('OREPO', 'path')

        incoming = "incoming"

        if args.version == LEDE_VERSION:
            self.tecos_dir = os.path.join(repo_path, "development", args.version,
                                          "packages", args.arch, "tecos", args.label)

            self.tecos_incoming_dir = os.path.join(self.tecos_dir, incoming)

        else:
            self.tecos_dir = os.path.join(repo_path, "development", args.version,
                                          args.arch, "tecos", args.label)

            self.tecos_incoming_dir = os.path.join(self.tecos_dir, incoming)

    def split_triplet(self, file_name):
        '''
        split_triplet function. Understanding by triplet <name> <version> <architecture>
        Args:
            Filename as package in ipk form
        Returns:
            Triplet separated by fields
        '''
        filename, extension = file_name.rsplit(".", 1)
        result = filename.split("_", 2)
        result.append(extension)

        return result

    def run(self):
        '''
        run function called when the program is executed
        '''
        self.processincoming(self.tecos_incoming_dir)

    def create_index(self, label):
        '''
        Generation of indexes for the given label
        Args: The label given by the process-incoming method
            {experimental|nightly|testing|stable}
        Returns:

        '''
        initial_path = os.getcwd()
        os.chdir(label)
        subprocess.call("create-opkg-index", shell=True)
        os.chdir(initial_path)

    def package_list(self, directory):
        '''
        Return the opkg packages list from a given directory
        Args:
            directory: Given directory.
        Returns:
            Opkg package list
        '''
        opackage_list = []
        if not os.path.exists(directory):
            print("Error missing directory {}".format(directory))
            sys.exit(-1)

        else:
            for the_file in os.listdir(directory):
                name, version, arch = the_file.split("_")
                package = OpkgPackage(name, version, arch)
                opackage_list.append(package)

        return opackage_list

    def delete_old_versions(self, new_package, pool):
        '''
        Delete old package version of a given package
        Args:
            new_package: Package to be compared.
            pool: the pool where the opkg will be deleted from
        '''
        if not os.path.exists(pool):
            print("Error missing pool {}".format(pool))

        else:
            if os.listdir(pool):
                for the_file in os.listdir(pool):
                    if os.path.isfile(os.path.join(pool, the_file)):
                        if the_file in ('Packages', 'Packages.gz'):
                            pass
                        else:
                            pkg_data = self.split_triplet(the_file)
                            if pkg_data[2] == args.arch and pkg_data[2] in ARCH_LIST:
                                if pkg_data[3] == "ipk":

                                    current_package = OpkgPackage(pkg_data[0], pkg_data[1],
                                                                  pkg_data[2], pkg_data[3])

                                    if current_package == new_package:

                                        if new_package.version > current_package.version:
                                            file_path = os.path.join(pool, the_file)
                                            os.remove(file_path)
                                        else:
                                            new_package.delete = True

    def remove(self, path):
        '''
        Delete a file or a directory recursively depending on the type itself
        Args:
            path The path to the file or directory
        '''
        if os.path.isdir(path):
            shutil.rmtree(path)

        elif os.path.isfile(path):
            os.remove(path)

    def processincoming(self, incoming):
        '''
        Process packages queue
        Args:
            incoming: incoming path where orepo will look for new uploads
        '''
        if os.listdir(incoming):
            for new_package in os.listdir(incoming):
                if not os.path.isdir(os.path.join(incoming, new_package)):

                    pkg_data = self.split_triplet(new_package)
                    if pkg_data[2] == args.arch and pkg_data[2] in ARCH_LIST:
                        if pkg_data[3] == "ipk":
                            new_package = OpkgPackage(pkg_data[0], pkg_data[1],
                                                      pkg_data[2], pkg_data[3])

                            file_path = os.path.join(incoming, new_package)

                            parent = os.path.abspath(os.path.join(incoming, os.pardir))
                            if parent:
                                self.delete_old_versions(new_package, parent)
                            if not new_package.delete:
                                shutil.copy(file_path, parent)
                                self.create_index(parent)
                self.remove(os.path.join(incoming, new_package))


if __name__ == "__main__":
    args = argparse.ArgumentParser(prog='orepo',
                                   usage='%(prog)s processincoming -v version -a arch -l label')
    args.add_argument('processincoming', help='Process packages queue')
    args.add_argument('-v', required=True, dest="version", default='17.01.4',
                      help='version {15.05|17.01.4}')
    args.add_argument('-a', required=True, dest="arch",
                      help='architecture {arm_cortex-a15_neon-vfpv4|'
                      'i386_pentium4|arm_cortex-a9_vfpv3}')
    args.add_argument('-l', required=True, dest="label",
                      default='nightly', help='label {experimental|nightly|testing}')
    args.add_argument('--t', action='store_true')
    args = args.parse_args()
    Orepo(args).run()
