#!/usr/bin/python3
# -*- coding: utf-8; mode: python -*-
"""
This file does the testing-pull operation of the openwrt repository
"""
import os
import sys
import shutil
import logging

REPO_PATH = '/var/repos/owrt/development/17.01.4/'
PACKAGES = 'packages'
ARCHS_LIST = ('arm_cortex-a15_neon-vfpv4', 'i386_pentium4')

TECOS = 'tecos'
NIGHTLY = 'nightly'
TESTING = 'testing'


logging.basicConfig(filename='/var/log/testing-pull/testing-pull.log',
                    filemode='w',
                    format='%(asctime)s [%(levelname)s] - %(message)s',
                    datefmt='%m/%d/%Y %I:%M:%S', level=logging.DEBUG)


def clear(dest):
    """
    Function to clear the path given by dest
    """
    file_list = os.listdir(dest)
    for the_file in file_list:
        os.remove(os.path.join(dest, the_file))


def pull(orig, dest):
    """
    Function to pull packages from testing to stable
    Args:
        orig: Origin path
        dest: Destination path
    """
    if not os.path.exists(dest):
        os.makedirs(dest)

    for the_file in os.listdir(orig):
        orig_file = os.path.join(orig, the_file)
        if os.path.isfile(orig_file):
            shutil.copy(orig_file, dest)
            logging.info('File %s promoted to stable', the_file)


for arch in ARCHS_LIST:

    orig_path = os.path.join(REPO_PATH, PACKAGES, arch, TECOS, NIGHTLY)
    dest_path = os.path.join(REPO_PATH, PACKAGES, arch, TECOS, TESTING)

    if not os.path.exists(orig_path):
        logging.error('Error missing directory')
        sys.exit(-1)

    else:
        clear(dest_path)
        pull(orig_path, dest_path)
