# -*- mode: makefile-gmake; coding: utf-8 -*-
DESTDIR?=~

BASH_DIR=src/bash
BIN_DIR=/usr/bin
all:

clean:
	$(RM) $(shell find -name *~)

install: manpage
	install -vd $(DESTDIR)/usr/bin
	install -v -m 755 src/orepo.py $(DESTDIR)/usr/bin/atenea

	install -v -m 755 src/testing-pull.py $(DESTDIR)/usr/bin/testing-pull
	install -v -m 755 src/stable-pull.py $(DESTDIR)/usr/bin/stable-pull

	install -v -m 755 $(BASH_DIR)/oprocess-incoming.sh $(DESTDIR)/usr/bin/oprocess-incoming
	install -v -m 755 $(BASH_DIR)/process-incoming.sh $(DESTDIR)/$(BIN_DIR)/process-incoming
	install -v -m 755 $(BASH_DIR)/create-opkg-index.sh $(DESTDIR)/$(BIN_DIR)/create-opkg-index
	install -v -m 755 $(BASH_DIR)/ipkg-make-index.sh $(DESTDIR)/$(BIN_DIR)/ipkg-make-index

	# porepo is still in python2
	install -v -m 755 src/porepo.py $(DESTDIR)/usr/bin/porepo

	# mirror scripts
	install -v -m 755 $(BASH_DIR)/alejandria-mirror.sh $(DESTDIR)/$(BIN_DIR)/alejandria-mirror
	install -v -m 755 $(BASH_DIR)/production-mirror.sh $(DESTDIR)/$(BIN_DIR)/production-mirror

	# Config files
	install -vd $(DESTDIR)/etc/orepo
	install -v -m 644 src/orepo.conf $(DESTDIR)/etc/orepo

	# Install manpages
	install -vd $(DESTDIR)/usr/share/man/man1

	install -v -m 644 atenea.1.gz $(DESTDIR)/usr/share/man/man1
	install -v -m 644 stable-pull.1.gz $(DESTDIR)/usr/share/man/man1
	install -v -m 644 testing-pull.1.gz $(DESTDIR)/usr/share/man/man1

# By now this is not needed it needs some fixing to support all the new architectures.
# Please do change to a config file before
	# install -v -m 755 $(BASH_DIR)/orepo-hierarchy.sh $(DESTDIR)/$(BIN_DIR)/orepo-hierarchy


manpage:
	gzip -9 < debian/atenea.1 > atenea.1.gz
	gzip -9 < debian/testing-pull.1 > testing-pull.1.gz
	gzip -9 < debian/stable-pull.1 > stable-pull.1.gz
